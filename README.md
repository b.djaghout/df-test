# Digital Factory skill test

## Content

This project contains two parts of one application

- Backend: Server application created using ExpressJS (Nodejs).
- Frontend: A ReactJS application that consume the backend API.

## How to launch this project?

_PS: You need to have a mongodb database installed in your machine or in your Docker, or just use railway.app PAAS (recommended)._

These are the steps you need to perform to test this project.

- First things first, clone the project to your local machine

```sh
git clone https://gitlab.com/b.djaghout/df-test.git
```

- Navigate in the project directory

```sh
cd df-skill-test
```

- Navigate into both front and back folders and install their dependencies:

```sh
cd front && npm i
```

```sh
cd ../back && npm i
```
- You need to create a .env file. Make a copy of .env.example and name it '.env'
- Next launch the backend
```sh
npm start
```
- In another terminal launch the frontend
```sh
cd ../front
npm start
```
- Finally visit http://localhost:3000, and enjoy .😃

Thank you for your patience!
