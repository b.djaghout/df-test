import { Application } from '../src/app';
import Audio from '../src/api/audio/entities/Audio';
import User from '../src/api/auth/entities/User';

const rawData = require('./data.json');

async function main() {
  const audioData = rawData.map((item: any) => ({
    name: item.trackName,
    album: item.collectionName || item.trackName,
    author: item.artistName,
    price: item.trackPrice,
    cover: item.artworkUrl100,
  }));

  const app = new Application();
  app.startServer();
  // Remove all document form Audio collection
  console.log('Emptying Audio collection...');
  await Audio.remove({});

  // Populate Audio collection
  console.log('Populating Audio Collection...');
  await Audio.create(audioData);

  await User.remove({});

  const createdUser = await User.create({
    firstname: 'admin',
    lastname: 'admin',
    username: 'admin',
    password: '123456',
    email: 'admin@email.dz',
    phone: '0699771374',
  });

  console.log({ createdUser });

  console.log('Done!');
  app.stopServer();
}

main();
