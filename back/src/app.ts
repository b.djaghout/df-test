import bodyParser from 'body-parser';
import express, { Express } from 'express';
import helmet from 'helmet';
import morgan from 'morgan';
import cors from 'cors';
import apiV1 from './api';
import './config/passport';
import { errorsHandler, four0four } from './exceptions/errors-helper';
import db from './config/db';

export class Application {
  private app: Express;
  constructor() {
    this.app = express();
    // Initiate the database
    db();
    // Set the Host and the port of the server
    this.app.set('host', process.env.HOST || 'localhost');
    this.app.set('port', process.env.PORT || 5000);

    // Helmet to add some security headers and remove x-powered-by
    this.app.use(helmet());

    // Morgan configuration (logging)
    this.app.use(morgan('combined'));

    // Extracting json data from requests body, parse it and put it in req.body
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    // Set cors headers
    this.app.use(cors());

    // Add API endpoints
    this.app.use('/api/v1', apiV1);

    // Welcome page
    this.app.get('/', (_, res) => {
      res.send('<h1>Welcome ⭐!</h1>');
    });

    // Setting 404 Error and Error Handler
    this.app.use(four0four);
    this.app.use(errorsHandler);
  }

  public startServer() {
    const host = this.app.get('host');
    const port = this.app.get('port');
    this.app.listen(port, host, () => {
      console.log(`Server started @ http://${host}:${port}`);
    });
  }

  public stopServer() {
    const host = this.app.get('host');
    const port = this.app.get('port');
    console.log(`Taking server @ http://${host}:${port} down!`);
    process.exit(0);
  }
}
