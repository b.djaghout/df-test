import { UserDocument } from "../../api/auth/entities/User";

declare global {
  namespace Express {
    interface User extends UserDocument {}
  }
}