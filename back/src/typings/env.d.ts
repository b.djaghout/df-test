declare namespace NodeJS {
    export interface ProcessEnv {
      DATABASE_URL: string,
      NODE_ENV: string;
      PORT: string;
      SECRET: string;
    }
  }