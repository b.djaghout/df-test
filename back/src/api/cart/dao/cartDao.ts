import Joi from 'joi';
import Cart from '../entities/Cart';
import Audio from '../../audio/entities/Audio';

function validateCreateCart(cartData: any) {
  const schema = Joi.object({
    userId: Joi.string().required(),
    items: Joi.array().items(Joi.string()),
  });
  const { value, error } = schema.validate(cartData);
  let formattedError;
  if (error) {
    formattedError = {
      message: 'Erreur de validation de donnée',
      fields: error.details.map((field) => ({
        field: field.path[0],
        message: field.message,
      })),
      code: 422,
    };
  }
  return { value, error: formattedError };
}

export const createCart = async (cartData: any) => {
  const { value, error } = validateCreateCart(cartData);
  if (error) {
    return {
      error: {
        message: 'Erreur de validation de donnée',
        fields: error,
        code: 422,
      },
    };
  }
  const audioDocs = await Audio.find({ _id: { $in: value.items } });

  const cart = await Cart.create({
    items: audioDocs,
    owner: value.userId,
  });
  return { value: cart.toJSON() };
};

export const getMyCheckouts = async (userId: string) => {
  try {
    const carts = await Cart.find({ owner: userId, status: 'PURCHASED' }).sort({ createdAt: -1 });
    return {
      value: carts.map((cart) => ({
        id: cart.toJSON().id,
        items: cart.toJSON().items,
        checkoutAddress: cart.toJSON().checkoutAddress,
        checkoutAt: cart.toJSON().checkoutAt,
        totalPrice: cart.toJSON().totalPrice,
      })),
    };
  } catch (error) {
    console.error(error);
    return { error };
  }
};

export const getCartById = async (cartId: string, ownerId: string) => {
  try {
    const cart = await Cart.findOne({ owner: ownerId, _id: cartId });
    if (!cart) {
      return {
        error: {
          message: 'Commande introuvable!',
          code: 404,
        },
      };
    }
    return { value: cart };
  } catch (error) {
    console.error(error);

    return { error };
  }
};

export const getCartByOwner = async (ownerId: string) => {
  try {
    const cart = await Cart.findOne({ owner: ownerId, status: 'PENDING' });
    if (!cart) {
      return {
        error: {
          message: 'Commande introuvable!',
          code: 404,
        },
      };
    }
    const value = cart.toJSON();
    return { value };
  } catch (error) {
    console.error(error);
    return { error };
  }
};

export const addAudioToCart = async (id: string, audioId: string) => {
  try {
    const cart = await Cart.findOne({ _id: id, status: 'PENDING' });
    if (!cart) {
      return {
        error: {
          message: 'Commande introuvable!',
          code: 404,
        },
      };
    }
    const audio = await Audio.findOne({ _id: audioId });
    if (!audio) {
      return {
        error: {
          message: 'Article introuvable!',
          code: 404,
        },
      };
    }
    await cart.addItem(audio);
    await cart.save();
    return { value: cart };
  } catch (error) {
    return { error };
  }
};

export const removeAudioFromCart = async (id: string, audioId: string) => {
  try {
    const cart = await Cart.findOne({ _id: id, status: 'PENDING' });
    if (!cart) {
      return {
        error: {
          message: 'Commande introuvable!',
          code: 404,
        },
      };
    }
    const audio = await Audio.findOne({ _id: audioId });
    if (!audio) {
      return {
        error: {
          message: 'Article introuvable!',
          code: 404,
        },
      };
    }
    await cart.removeItemFromCart(audio);
    const res = await cart.save();
    return { value: res.toJSON() };
  } catch (error) {
    return { error };
  }
};

export const checkoutCart = async (cartId: string, address: string) => {
  try {
    const cart = await Cart.findOne({ _id: cartId, status: 'PENDING' });
    if (!cart) {
      return {
        error: {
          message: 'Commande introuvable!',
          code: 404,
        },
      };
    }
    cart.status = 'PURCHASED';
    cart.checkoutAddress = address;
    cart.checkoutAt = new Date();
    const res = await cart.save();
    const value = res.toJSON();
    return { value };
  } catch (error) {
    return { error };
  }
};
