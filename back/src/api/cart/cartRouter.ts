import { Request, Response, Router } from 'express';
import passport from 'passport';
import {
  addAudioToCart,
  checkoutCart,
  createCart,
  getCartById,
  getCartByOwner,
  getMyCheckouts,
  removeAudioFromCart,
} from './dao/cartDao';

const cartRouter = Router();

cartRouter.post('', passport.authenticate('jwt', { session: false }), async (req: Request, res: Response) => {
  const { value, error } = await createCart({
    ...req.body,
    userId: req.user?.id,
  });
  if (error) {
    return res.status(error.code || 400).json(error);
  }
  return res.status(201).json(value);
});

cartRouter.get('/checkout', passport.authenticate('jwt', { session: false }), async (req: Request, res: Response) => {
  const { value, error } = await getMyCheckouts(req.user?.id);
  if (error) {
    return res.status(error.code || 400).json(error);
  }
  return res.status(200).json(value);
});

cartRouter.get('/my-cart', passport.authenticate('jwt', { session: false }), async (req: Request, res: Response) => {
  const ownerId = req.user?.id;
  const { error, value } = await getCartByOwner(ownerId);
  if (error) {
    return res.status(error.code || 400).json(error);
  }
  return res.status(200).json(value);
});

cartRouter.post(
  '/:id/checkout',
  passport.authenticate('jwt', { session: false }),
  async (req: Request, res: Response) => {
    const { id } = req.params;
    const address = req.query.address as string;
    const { error, value } = await checkoutCart(id, address);
    if (error) {
      return res.status(error.code || 400).json(error);
    }
    return res.status(200).json(value);
  },
);

cartRouter.post(
  '/:id/items/:audioId',
  passport.authenticate('jwt', { session: false }),
  async (req: Request, res: Response) => {
    const { id, audioId } = req.params;
    const { error, value } = await addAudioToCart(id, audioId);
    if (error) {
      return res.status(error.code || 400).json(error);
    }
    return res.status(200).json(value);
  },
);

cartRouter.delete(
  '/:id/items/:audioId',
  passport.authenticate('jwt', { session: false }),
  async (req: Request, res: Response) => {
    const { id, audioId } = req.params;
    const { error, value } = await removeAudioFromCart(id, audioId);
    if (error) {
      return res.status(error.code || 400).json(error);
    }
    return res.status(200).json(value);
  },
);

cartRouter.get('/:id', passport.authenticate('jwt', { session: false }), async (req: Request, res: Response) => {
  const ownerId = req.user?.id;
  const cartId = req.params['id'];
  const { value, error } = await getCartById(cartId, ownerId);
  if (error) {
    return res.status(error.code || 400).json(error);
  }
  return res.status(200).json(value);
});

export default cartRouter;
