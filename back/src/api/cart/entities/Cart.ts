import mongoose, { Document, ObjectId, Schema } from 'mongoose';
import { AudioDoc } from '../../audio/entities/Audio';

export interface CartDoc extends Document {
  id: string;
  items: Array<any>;
  totalPrice: number;
  status: 'PENDING' | 'CANCELED' | 'PURCHASED';
  owner: ObjectId;
  checkoutAddress: string;
  checkoutAt: Date;
  addItem(audio: any): Promise<any>;
  removeItemFromCart(audio: AudioDoc): Promise<any>;
}

const cartSchema = new Schema<CartDoc>(
  {
    items: {
      type: [{}],
      required: true,
    },
    totalPrice: {
      type: Number,
      required: false,
    },
    status: {
      type: String,
      default: 'PENDING',
    },
    owner: {
      ref: 'User',
      type: mongoose.Types.ObjectId,
      required: true,
    },
    checkoutAddress: {
      type: String,
      required: false,
    },
    checkoutAt: {
      type: Date,
      required: false,
    },
  },
  { timestamps: true },
);

cartSchema.pre<CartDoc>('save', async function (next) {
  const cart = this;
  this.totalPrice = cart.items.reduce((total, item) => total + item.price, 0);
  next();
});

cartSchema.methods.addItem = async function (audio: any) {
  const cart = this;
  if (!cart.items.some((item: any) => String(item._id) === String(audio._id))) {
    cart.items.push(audio);
  }
  return cart;
};

cartSchema.methods.removeItemFromCart = async function (audio: AudioDoc) {
  const cart = this;
  cart.items = cart.items.filter((item: any) => {
    return String(item._id) !== String(audio.id);
  });
  return cart;
};

cartSchema.set('toJSON', {
  transform: (_doc, res) => {
    res.id = res._id;
    delete res._id;
    delete res.__v;
    res.items = res.items.map((audio: any) => {
      audio.id = audio._id || audio.id;
      delete audio._id;
      delete audio.__v;
      return audio;
    });
  },
});

export default mongoose.model<CartDoc>('Cart', cartSchema);
