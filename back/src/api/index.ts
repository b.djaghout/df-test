import { Router } from "express";
import audioRouter from "./audio/audioRouter";
import userRouter from "./auth/auth";
import cartRouter from "./cart/cartRouter";

const api = Router();

// Register all the api endpoints here
api.use("", userRouter);
api.use("/audios", audioRouter);
api.use("/carts", cartRouter);

export default api;
