import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import mongoose, { Document } from 'mongoose';
import Token from './Token';

export type UserDocument = Document & {
  id: string;
  firstname: string;
  lastname: string;
  phone: string;
  username: string;
  email: string;
  password: string;
  lastAccess: Date;
  isValidPassword(password: string): Promise<boolean>;

  isAuthenticated(): Promise<boolean>;
};

const userSchema = new mongoose.Schema(
  {
    firstname: {
      type: 'string',
      required: true,
      trim: true,
    },
    lastname: {
      type: 'string',
      required: true,
      trim: true,
    },
    phone: {
      type: 'string',
      required: true,
      unique: true,
      trim: true,
    },
    username: {
      type: 'string',
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },
    email: {
      type: 'string',
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
    },
    password: {
      type: 'string',
      required: true,
      trim: true,
    },
    lastAccess: {
      type: Date,
      default: Date.now(),
    },
  },
  { timestamps: true },
);

userSchema.pre<UserDocument>('save', async function (next) {
  const user = this;
  this.password = await argon2.hash(user.password);
  next();
});

userSchema.methods.isAuthenticated = async function isAuthenticated() {
  const token = await Token.findOne({ userId: this.id });
  if (!token) {
    return false;
  }
  const decodedToken: any = jwt.verify(token.id, String(process.env.SECRET), {});
  if (decodedToken.user) {
    return true;
  }
  await Token.deleteMany({ userId: this.id });
  return false;
};

userSchema.methods.isValidPassword = async function (password: string) {
  const user = this;
  return argon2.verify(user.password, password);
};

export default mongoose.model<UserDocument>('User', userSchema);
