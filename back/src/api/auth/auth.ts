import { NextFunction, Request, Response, Router } from 'express';
import passport from 'passport';
import User from './entities/User';
import { logoutUser, validateLoginData, validateSignupData } from './dao/userDao';

const userRouter = Router();

userRouter.post('/register', async (req: Request, res: Response, next: Function) => {
  const { error } = validateSignupData(req.body);
  if (error) {
    return res.status(422).json(error);
  }

  const userData = [{ phone: req.body.phone }, { email: req.body.email }, { username: req.body.username }];
  const userExists = await User.count({ $or: userData });

  if (userExists) {
    const redirectUrl = `/`;
    return res.redirect(redirectUrl);
  }

  return passport.authenticate('register', { session: false }, (error, token) => {
    if (error) {
      return res.status(error.code || 400).json({ error });
    }
    return res.json(token);
  })(req, res, next);
});

userRouter.post('/login', async (req: Request, res: Response, next: Function) => {
  const { error } = validateLoginData(req.body);
  if (error) {
    return res.status(422).json(error);
  }
  return passport.authenticate('login', { session: false }, (error, token) => {
    if (error) {
      return res.status(error.code || 400).json(error);
    }
    return res.json(token);
  })(req, res, next);
});

userRouter.post('/logout', (req: Request, res: Response, next: NextFunction) => {
  passport.authenticate('jwt', { session: false }, async (error, user) => {
    if (error) {
      return res.status(401).json({
        code: 401,
        message: 'Unauthorized',
      });
    }
    try {
      await logoutUser(user);
      res.status(200).json({ message: 'logout successfully' });
    } catch (err) {
      return next(err);
    }
  })(req, res, next);
});

export default userRouter;
