import Joi from 'joi';
import Token from '../entities/Token';
import User from '../entities/User';

export function validateSignupData(userData: any) {
  const schema = Joi.object({
    firstname: Joi.string().min(1).required().messages({
      'any.required': 'Le prénom est obligatoire!',
      'string.empty': 'Le prénom est obligatoire!',
    }),
    lastname: Joi.string().min(1).required().messages({
      'any.required': 'Le nom est obligatoire!',
      'string.empty': 'Le nom est obligatoire!',
    }),
    phone: Joi.string()
      .pattern(/^0[5-7][4-9]\d{7}$/)
      .required()
      .messages({
        'string.pattern.base': 'Le numéro de téléphone est érroné!',
        'string.empty': 'Le numéro de téléphone est obligatoire!',
        'any.required': 'Le numéro de téléphone est obligatoire!',
      }),
    username: Joi.string().min(3).required().messages({
      'any.required': 'Username est obligatoire!',
      'string.empty': 'Username est obligatoire!',
      'any.min': 'Username doit etre au moin 3 caractère!',
    }),
    email: Joi.string().email({ minDomainSegments: 2 }).required().messages({
      'any.required': 'Email est obligatoire!',
      'string.empty': 'Email est obligatoire!',
      'string.email': 'Email est invalide',
    }),
    password: Joi.string().min(6).required().messages({
      'any.required': 'Le mot de passe est requis',
      'string.empty': 'Le mot de passe est requis',
      'string.min': 'Le mot de passe doit contenir au moin 6 caractères',
    }),
  });
  const { value, error } = schema.validate(userData, { abortEarly: false });
  let formattedError;
  if (error) {
    formattedError = {
      message: 'Erreur de validation de donnée',
      fields: error.details.map((field) => ({
        field: field.path[0],
        message: field.message,
      })),
      code: 422,
    };
  }
  return { value, error: formattedError };
}

export function validateLoginData(userData: any) {
  const schema = Joi.object({
    email: Joi.string().email({ minDomainSegments: 2 }).lowercase().messages({
      'any.required': 'Email est obligatoire!',
      'string.empty': 'Email est obligatoire!',
      'string.email': 'Email invalide',
    }),
    username: Joi.alternatives().try(
      Joi.string().min(2).lowercase(),
      Joi.string().email({ minDomainSegments: 2 }).lowercase().messages({
        'string.email': 'Email invalide',
      }),
    ),
    password: Joi.string().min(6).required().messages({
      'any.required': 'Le mot de passe est obligatoire!',
      'string.empty': 'Le mot de passe est obligatoire!',
      'string.min': 'Le mot de passe doit contenir au moin 6 caractères',
    }),
  }).or('email', 'username');

  const { value, error } = schema.validate(userData, { abortEarly: false });

  let formattedError;
  if (error) {
    formattedError = {
      message: 'Erreur de validation de donnée',
      fields: error.details.map((field) => ({
        field: field.path[0],
        message: field.message,
      })),
      code: 422,
    };
  }
  return { value, error: formattedError };
}

async function createToken(userIdParam: string, lastAccess: Date, firstname: string, lastname: string): Promise<any> {
  const { id, createdAt, ttl, userId } = await Token.create({
    userId: userIdParam,
  });
  await User.updateOne({ _id: userIdParam }, { lastAccess: Date.now() });
  return { token: { id, createdAt, ttl, userId, lastAccess, firstname, lastname } };
}

export const registerUser = async (userData: any): Promise<{ error?: any; token?: Joi.ValidationError }> => {
  const { value, error } = validateSignupData(userData);

  if (error) {
    return { error };
  }
  const user = await User.create({
    ...value,
  }).catch((err: any) => {
    throw err;
  });
  return createToken(user.id, user.lastAccess, user.firstname, user.lastname);
};

export const loginUser = async (loginData: any): Promise<{ error?: any; token?: any }> => {
  const { value, error } = validateLoginData(loginData);
  if (error) {
    return {
      error: {
        message: 'Erreur de validation de donnée',
        fields: error,
        code: 422,
      },
    };
  }
  const searchUser = {
    $or: [{ email: value.username }, { username: value.username }],
  };
  const user = await User.findOne(searchUser);
  const authError = {
    code: 400,
    message: "Informations d'identification incorrectes",
  };
  if (!user) {
    return { error: authError };
  }
  const isMatch = await user.isValidPassword(value.password);
  if (!isMatch) {
    return { error: authError };
  }
  return createToken(user.id, user.lastAccess, user.firstname, user.lastname);
};

export const logoutUser = async (user: any): Promise<boolean> => {
  const res = await Token.deleteMany({ userId: user.id });
  return !!res.deletedCount;
};
