import { Request, Response, Router } from 'express';
import { createAudio, getAudios } from './dao/audioDao';
import { AudioDoc } from './entities/Audio';

const audioRouter = Router();

audioRouter.post<{}, { data: AudioDoc }, Partial<AudioDoc>, {}>('', async (req: Request, res: Response) => {
  const { value, error } = await createAudio(req.body);
  if (error) {
    res.status(error.code || 400).json(error);
  }
  return res.status(200).json(value);
});

audioRouter.get('', async (req: Request, res: Response) => {
  const { value, error } = await getAudios(String(req.query.search));
  if (error) {
    res.status(error.code || 400).json(error);
  }
  return res.status(200).json(value);
});

export default audioRouter;
