import mongoose from "mongoose";

export interface AudioDoc extends Document {
  id: string;
  name: string;
  album: string;
  author: string;
  price: number;
  cover?: string;
}

const audioSchema = new mongoose.Schema<AudioDoc>(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    album: {
      type: String,
      required: true,
      trim: true,
    },
    author: {
      type: String,
      required: true,
      trim: true,
    },
    price: {
      type: Number,
      required: true,
      trim: true,
    },
    cover: {
      type: String,
      required: true,
      trim: true,
    },
  },
  { timestamps: true }
);

audioSchema.set("toJSON", {
  transform: (_document, returnedObject) => {
    returnedObject.id = returnedObject._id.toString();
    delete returnedObject._id;
    delete returnedObject.__v;
  },
});

export default mongoose.model<AudioDoc>("Audio", audioSchema);
