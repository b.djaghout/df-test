import Joi from 'joi';
import Audio from '../entities/Audio';

export const validateAudio = (audioData: any) => {
  const schema = Joi.object({
    name: Joi.string().required().messages({
      'any.requried': 'Nom est obligatoire!',
      'string.empty': 'Nom est obligatoire!',
    }),
    album: Joi.string().required().messages({
      'any.requried': 'Albume est obligatoire!',
      'string.empty': 'Albume est obligatoire!',
    }),
    author: Joi.string().required().messages({
      'any.requried': "L'autuer est obligatoire!",
      'string.empty': "L'autuer est obligatoire!",
    }),
    price: Joi.number().min(0).default(0).messages({
      'any.min': 'Le prix doit être supèrieur ou égale a 0!',
    }),
    cover: Joi.string().uri({ allowRelative: false }),
  });

  const { value, error } = schema.validate(audioData, { abortEarly: false });
  let formattedError;
  if (error) {
    formattedError = {
      message: 'Erreur de validation de donnée',
      fields: error.details.map((field) => ({
        field: field.path[0],
        message: field.message,
      })),
      code: 422,
    };
  }
  return { value, error: formattedError };
};

export const createAudio = async (audioData: any) => {
  const { value, error } = validateAudio(audioData);
  if (error) {
    return {
      error: {
        message: 'Erreur de validation de donnée',
        fields: error,
        code: 422,
      },
    };
  }
  const audio = await Audio.create({ ...value }).catch((err) => {
    throw err;
  });
  return { value: audio.toJSON() };
};

export const getAudios = async (search: string = '') => {
  try {
    const searchCondition = {
      $or: [
        {name: new RegExp(search, 'i')},
        {album: new RegExp(search, 'i')},
        {author: new RegExp(search, 'i')}
      ],
    };
    const audios = await Audio.find(searchCondition);
    return { value: audios };
  } catch (error) {
    return { error };
  }
};
