import mongoose from "mongoose";
import HttpException from "../exceptions/HttpException";

export default async () => {
  try {
    await mongoose.connect(String(process.env.DATABASE_URL), {
      dbName: "dfSkillTest",
    });
  } catch (err) {
    throw new HttpException(500, "Erreur de connexion avec la base de donnée");
  }
};
