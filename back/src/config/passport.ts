import {Request} from "express";
import passport from "passport";
import {ExtractJwt, Strategy as JWTStrategy} from "passport-jwt";
import {Strategy as LocalStrategy} from "passport-local";
import {loginUser, registerUser} from "../api/auth/dao/userDao";
import User from "../api/auth/entities/User";

passport.use(
    "register",
    new LocalStrategy(
        {
            usernameField: "email",
            passwordField: "password",
            passReqToCallback: true,
        },
        async (req: any, _email, _password, done) => {
            try {
                const {token, error} = await registerUser(req.body);
                if (error) {
                    return done(error);
                } else {
                    req.token = token;
                    return done(null, token);
                }
            } catch (err) {
                done(err);
            }
        }
    )
);

passport.use(
    "login",
    new LocalStrategy(
        {
            passReqToCallback: true,
        },
        async (req: Request, _email, _password, done) => {
            try {
                const {token, error} = await loginUser(req.body);
                if (error) {
                    return done(error);
                }
                done(null, token);
            } catch (err) {
                done(err);
            }
        }
    )
);

passport.use(
    new JWTStrategy(
        {
            secretOrKey: String(process.env.SECRET),
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        },
        async (token, done) => {
            try {
                const user = await User.findOne({_id: token.user.id});
                if (!user) {
                    return done(new Error("Unauthorized"));
                }
                const isAuthenticated = await user.isAuthenticated();
                if (!isAuthenticated) {
                    return done(new Error("Unauthorized"));
                }
                return done(null, user);
            } catch (err) {
                done(err);
            }
        }
    )
);
