import React from "react";
import { Box, Typography } from "@mui/material";
import { Link } from "react-router-dom";

const NotFound = () => {
  return (
    <Box
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "100vh",
        flexDirection: "column",
        backgroundColor: "lightgreen",
      }}
    >
      <Typography variant="h1" style={{ color: "white", display: "block" }}>
        404
      </Typography>
      <Typography variant="h4" style={{ color: "white" }}>
        Vous avez perdu votre chemin ?
        <Link to="/" style={{ textDecoration: "none" }}>
          par ici!
        </Link>
      </Typography>
    </Box>
  );
};

export default NotFound;
