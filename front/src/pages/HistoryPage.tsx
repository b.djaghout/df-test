import React, { useEffect, useState } from "react";
import { AppBar, Toolbar } from "@mui/material";
import { Logo, NavMenu, ShoppingCartWithBadge } from "./shared";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { Cart } from "../models/Cart";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import CircularProgress from "@mui/material/CircularProgress/CircularProgress";
import { getMyCheckout } from "../carts/cartService";
import relativeTime from "dayjs/plugin/relativeTime";
import "dayjs/locale/fr";
import dayjs from "dayjs";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import { ExpandMore } from "@mui/icons-material";
import { useAppSelector } from "../hooks";

dayjs.extend(relativeTime);

const HistoryPage = () => {
  const count = useAppSelector((state) => state.cartState.count);
  const [carts, setCarts] = useState<[Cart?]>([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setLoading(true);
    getMyCheckout()
      .then(({ error, value }) => {
        if (error) {
          return console.log(error);
        }
        setCarts(value);
      })
      .finally(() => setLoading(false));
  }, []);

  const [expanded, setExpanded] = useState<string | false>(false);

  const handleChange =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };

  const CartTag = ({ cart }: { cart?: Cart }) => {
    const cartItemStyle = {
      alignItems: "center",
      padding: 1,
      margin: 1,
      borderRadius: "10px",
      backgroundColor: "rgba(240, 240, 240, .5)",
      transition: "background 200ms ease-in-out",
      "&:hover": {
        background: "rgba(240, 240, 240, .9)",
      },
    };

    return (
      <Accordion
        expanded={expanded === cart?.id}
        onChange={handleChange(cart!.id)}
      >
        <AccordionSummary expandIcon={<ExpandMore />}>
          <Grid container>
            <Grid item xs={2}>
              <Typography>
                {dayjs(cart?.checkoutAt).locale("fr").fromNow()}
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography>
                {"DZD "}
                {cart?.totalPrice.toLocaleString(undefined, {
                  maximumFractionDigits: 2,
                })}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography>{cart?.checkoutAddress}</Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography>{cart?.items?.length || 0} element(s)</Typography>
            </Grid>
          </Grid>
        </AccordionSummary>
        <AccordionDetails>
          {cart?.items?.map((item) => (
            <Grid container key={item.id} sx={cartItemStyle}>
              <Grid item sx={{ marginRight: 1 }}>
                <img
                  src={item.cover}
                  alt="Album cover"
                  style={{ borderRadius: "10px" }}
                />
              </Grid>
              <Grid item sx={{ flexGrow: 1 }}>
                <Typography variant="h6" sx={{ display: "block" }}>
                  {item.name}
                </Typography>
                <Typography variant="h6" sx={{ display: "block" }}>
                  {item.album}
                </Typography>
                <Typography variant="subtitle1" sx={{ display: "block" }}>
                  {item.author}
                </Typography>
                <Typography variant="subtitle2" sx={{ display: "block" }}>
                  DZD {item.price}
                </Typography>
              </Grid>
            </Grid>
          ))}
        </AccordionDetails>
      </Accordion>
    );
  };

  return (
    <Box sx={{ marginTop: 10 }}>
      <AppBar position="fixed">
        <Toolbar sx={{ display: "flex", justifyContent: "space-between" }}>
          <Logo text="APPLE" />
          <Grid container>
            <ShoppingCartWithBadge count={count} />
            <NavMenu />
          </Grid>
        </Toolbar>
      </AppBar>
      {loading && (
        <Grid
          container
          sx={{
            justifyContent: "center",
            alignItems: "center",
            height: "calc(100vh - 64px)",
          }}
        >
          <CircularProgress color="primary" size={80} />
        </Grid>
      )}
      {!loading && (
        <Grid container>
          <Container maxWidth="xl">
            <Typography variant="h3">Historique d'achat</Typography>
            {carts?.length === 0 && (
              <Grid
                item
                xs={12}
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  justifyContent: "center",
                  alignItems: "center",
                  height: "25vh",
                }}
              >
                <Typography variant="h4" sx={{ color: "lightgray" }}>
                  Il n'y a aucun achat
                </Typography>
              </Grid>
            )}
            {!!carts.length &&
              carts.map((cart) => <CartTag cart={cart} key={cart?.id} />)}
          </Container>
        </Grid>
      )}
    </Box>
  );
};

export default HistoryPage;
