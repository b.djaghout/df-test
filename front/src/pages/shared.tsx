import {
  Logout,
  Settings,
  ShoppingBagOutlined,
  ShoppingCart
} from "@mui/icons-material";
import {
  Badge,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Tooltip,
  Typography,
} from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import React, { FormEvent } from "react";
import { Link, useNavigate } from "react-router-dom";
import { getToken, logout } from "../auth/authService";

export const Logo = ({ text }: { text?: string }) => {
  return (
    <Link to="/" style={{ color: "white", textDecoration: "none" }}>
      <Typography
        variant="h5"
        noWrap
        sx={{
          mr: 2,
          fontFamily: "monospace",
          fontWeight: 700,
          letterSpacing: ".3rem",
          color: "inherit",
          textDecoration: "none",
        }}
      >
        {text}
      </Typography>
    </Link>
  );
};

export const NavMenu = () => {
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = async () => {
    await logout();
    navigate("/login", { replace: true });
  };

  const handleNavigateToHistory = () => {
    navigate("/history", { replace: true });
  };

  return (
    <React.Fragment>
      <Box sx={{ display: "flex", alignItems: "center", textAlign: "center" }}>
        <Tooltip title="Gestion">
          <IconButton
            onClick={handleClick}
            size="small"
            sx={{ ml: 2 }}
            aria-controls={open ? "account-menu" : undefined}
            aria-haspopup="true"
            aria-expanded={open ? "true" : undefined}
          >
            <Settings sx={{ width: "32px", height: "32px", color: "white" }} />
          </IconButton>
        </Tooltip>
      </Box>
      <Menu
        anchorEl={anchorEl}
        id="account-menu"
        open={open}
        onClose={handleClose}
        onClick={handleClose}
        PaperProps={{
          elevation: 0,
          sx: {
            overflow: "visible",
            filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
            mt: 1.5,
            "& .MuiAvatar-root": {
              width: 32,
              height: 32,
              ml: -0.5,
              mr: 1,
            },
            "&:before": {
              content: '""',
              display: "block",
              position: "absolute",
              top: 0,
              right: 14,
              width: 10,
              height: 10,
              bgcolor: "background.paper",
              transform: "translateY(-50%) rotate(45deg)",
              zIndex: 0,
            },
          },
        }}
        transformOrigin={{ horizontal: "right", vertical: "top" }}
        anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
      >
        <MenuItem onClick={handleNavigateToHistory}>
          <ListItemIcon>
            <ShoppingBagOutlined
              fontSize="medium"
              sx={{ color: "dodgerblue" }}
            />
          </ListItemIcon>
          Historiques d'achat
        </MenuItem>
        <MenuItem onClick={handleLogout}>
          <ListItemIcon>
            <Logout fontSize="medium" sx={{ color: "firebrick" }} />
          </ListItemIcon>
          Se déconnecter
        </MenuItem>
      </Menu>
    </React.Fragment>
  );
};

export const ShoppingCartWithBadge = ({ count }: { count?: number }) => {
  const navigate = useNavigate();
  const handleCart = () => {
    navigate("/cart", { replace: true });
  };

  return (
    <Box sx={{ display: "flex", flex: 1, justifyContent: "end" }}>
      <IconButton sx={{ color: "white" }} onClick={handleCart}>
        <Badge badgeContent={count} color="error">
          <ShoppingCart />
        </Badge>
      </IconButton>
    </Box>
  );
};

interface MyDialogProps {
  open: boolean;
  onConfirm: () => any;
  onClose: () => any;
}

export const DeleteDialog = ({ open, onConfirm, onClose }: MyDialogProps) => {
  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Supprimer element</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Voulez-vous vraiment supprimer cette element?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onConfirm} autoFocus color="error">
            OK
          </Button>
          <Button onClick={onClose}>Annuler</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export const CheckoutDialog = ({ open, onConfirm, onClose }: any) => {
  const token = getToken();
  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const checkoutData = {
      firstname: data.get("firstname"),
      lastname: data.get("lastname"),
      address: data.get("address"),
    };
    onConfirm(checkoutData);
  };
  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Supprimer element</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {"Voulez-vous confirmer vos achats?"}
          </DialogContentText>
          <Box component="form" onSubmit={handleSubmit}>
            <Grid container spacing={2} sx={{ my: 2 }}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="firstname"
                  label="Prénom"
                  variant="outlined"
                  value={token?.firstname}
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="lname"
                  name="lastname"
                  label="Nom"
                  variant="outlined"
                  value={token?.lastname}
                  fullWidth
                  required
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  autoComplete="address"
                  name="address"
                  label="Adresse"
                  variant="outlined"
                  fullWidth
                  required
                />
              </Grid>
              <Grid
                item
                xs={12}
                sx={{ display: "flex", justifyContent: "flex-end", gap: 1 }}
              >
                <Button
                  type="submit"
                  variant="contained"
                  autoFocus
                  color="primary"
                >
                  Oui
                </Button>
                <Button onClick={onClose} variant="outlined">
                  Non
                </Button>
              </Grid>
            </Grid>
          </Box>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export const ThanksDialog = ({
  open,
  onClose,
  clientData,
}: {
  open: boolean;
  onClose: any;
  clientData: any;
}) => {
  const fullName = `${clientData.lastname} ${clientData.firstname}`;
  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Achat réussi</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Merci {fullName} pour avoir acheté les chansons chez Apple
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} autoFocus color="success">
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
