import { Delete, ShoppingCartCheckout } from "@mui/icons-material";
import { AppBar, Toolbar } from "@mui/material";
import Box from "@mui/material/Box";
import React, { useEffect, useState } from "react";
import {
  Logo,
  DeleteDialog,
  NavMenu,
  CheckoutDialog,
  ThanksDialog,
} from "./shared";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import { useAppDispatch, useAppSelector } from "../hooks";
import IconButton from "@mui/material/IconButton";
import {
  fetchCart,
  removeFromCart,
  removeAllFromCart,
  cartCheckout,
} from "../reducers/cartSlice";
import CircularProgress from "@mui/material/CircularProgress";
import { Album } from "../models/Album";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
import { getToken } from "../auth/authService";

const CartPage = () => {
  const [open, setOpen] = useState(false);
  const [openCheckout, setOpenCheckout] = useState(false);
  const [openThanks, setOpenThanks] = useState(false);
  const [albumToDelete, setAlbumToDelete] = useState<Album | undefined>(
    undefined
  );
  const dispatcher = useAppDispatch();
  useEffect(() => {
    dispatcher(fetchCart());
  }, [dispatcher]);
  const cart = useAppSelector((state) => state.cartState.cart);
  const loading = useAppSelector((state) => state.cartState.loading);

  const token = getToken();

  const clientData = {
    firstname: token?.firstname,
    lastname: token?.lastname,
  };

  const cartItemStyle = {
    alignItems: "center",
    padding: 1,
    margin: 1,
    borderRadius: "10px",
    backgroundColor: "rgba(240, 240, 240, .5)",
    transition: "background 200ms ease-in-out",
    "&:hover": {
      background: "rgba(240, 240, 240, .9)",
    },
  };

  const handleDelete = (album: Album) => {
    setAlbumToDelete(album);
    setOpen(true);
  };

  const handleConfirm = () => {
    dispatcher(removeFromCart(albumToDelete!));
    handleClose();
  };

  const handleClose = () => {
    setAlbumToDelete(undefined);
    setOpen(false);
    setOpenCheckout(false);
    setOpenThanks(false);
  };

  const checkout = () => {
    setOpenCheckout(true);
  };

  const handleCheckout = (checkoutData: any) => {
    dispatcher(
      cartCheckout({ cartId: cart.id, address: checkoutData.address })
    );
    dispatcher(removeAllFromCart());
    setOpenCheckout(false);
    setOpenThanks(true);
  };

  return (
    <Box sx={{ marginTop: 10 }}>
      <AppBar position="fixed">
        <Toolbar sx={{ display: "flex", justifyContent: "space-between" }}>
          <Logo text="APPLE" />
          <NavMenu />
        </Toolbar>
      </AppBar>
      {loading && (
        <Grid
          container
          sx={{
            justifyContent: "center",
            alignItems: "center",
            height: "calc(100vh - 64px)",
          }}
        >
          <CircularProgress color="primary" size={80} />
        </Grid>
      )}
      {!loading && (
        <Grid container sx={{ display: "flex", justifyContent: "center" }}>
          <Container maxWidth="lg">
            <Typography variant="h3">Vos achats</Typography>
            {cart.items && cart.items.length > 0 && (
              <Grid container sx={{ justifyContent: "space-between" }}>
                <Typography variant="h4">
                  Total: DZD{" "}
                  {cart.totalPrice.toLocaleString(undefined, {
                    maximumFractionDigits: 2,
                  })}
                </Typography>
                <Button
                  variant="outlined"
                  startIcon={<ShoppingCartCheckout />}
                  onClick={checkout}
                >
                  Checkout
                </Button>
              </Grid>
            )}
            <Grid container>
              {(!cart.items || cart.items.length === 0) && (
                <Grid
                  container
                  spacing={4}
                  sx={{
                    justifyContent: "space-around",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <Grid item>
                    <Typography
                      variant="h4"
                      sx={{ color: "rgba(100, 100, 100, .8)" }}
                    >
                      Le panier est vide
                    </Typography>
                  </Grid>
                  <Grid item>
                    <Button
                      variant="outlined"
                      sx={{
                        fontSize: "2rem",
                        marginY: 2,
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <Link
                        to="/"
                        style={{
                          color: "dodgerblue",
                          textDecoration: "none",
                        }}
                      >
                        <ShoppingCartCheckout fontSize="inherit" />
                        <Typography>Retour au magasinage</Typography>
                      </Link>
                    </Button>
                  </Grid>
                </Grid>
              )}
              {cart.items?.map((item) => (
                <Grid container key={item.id} sx={cartItemStyle}>
                  <Grid item sx={{ marginRight: 1 }}>
                    <img
                      src={item.cover}
                      alt="Album cover"
                      style={{ borderRadius: "10px" }}
                    />
                  </Grid>
                  <Grid item sx={{ flexGrow: 1 }}>
                    <Typography variant="h6" sx={{ display: "block" }}>
                      {item.name}
                    </Typography>
                    <Typography variant="h6" sx={{ display: "block" }}>
                      {item.album}
                    </Typography>
                    <Typography variant="subtitle1" sx={{ display: "block" }}>
                      {item.author}
                    </Typography>
                    <Typography variant="subtitle2" sx={{ display: "block" }}>
                      DZD {item.price}
                    </Typography>
                  </Grid>
                  <Grid item>
                    <IconButton size="large" onClick={() => handleDelete(item)}>
                      <Delete fontSize="inherit" color="error" />
                    </IconButton>
                  </Grid>
                </Grid>
              ))}
              {cart.items && cart.items.length > 0 && (
                <Grid container sx={{ justifyContent: "space-between" }}>
                  <Typography variant="h4">
                    Total: DZD{" "}
                    {cart.totalPrice.toLocaleString(undefined, {
                      maximumFractionDigits: 2,
                    })}
                  </Typography>
                  <Button
                    variant="outlined"
                    startIcon={<ShoppingCartCheckout />}
                    onClick={checkout}
                  >
                    Checkout
                  </Button>
                </Grid>
              )}
            </Grid>
          </Container>
        </Grid>
      )}
      <DeleteDialog
        open={open}
        onConfirm={handleConfirm}
        onClose={handleClose}
      />
      <CheckoutDialog
        open={openCheckout}
        onConfirm={handleCheckout}
        onClose={handleClose}
      />
      <ThanksDialog
        open={openThanks}
        onClose={handleClose}
        clientData={clientData}
      />
    </Box>
  );
};

export default CartPage;
