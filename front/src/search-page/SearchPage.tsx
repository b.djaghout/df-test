import "./SearchPage.css";
import { Close, Search as SearchIcon, ShoppingCart } from "@mui/icons-material";
import {
  AppBar,
  Badge,
  CircularProgress,
  Divider,
  Grid,
  IconButton,
  InputBase,
  Paper,
  Toolbar,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import React, { useEffect, useState } from "react";
import { Album } from "../models/Album";
import { isLoggedIn } from "../auth/authService";
import { getAllAlbums, searchAlbums } from "../albums/albumsService";
import { useAppDispatch, useAppSelector } from "../hooks";
import { addToCart, fetchCart, removeFromCart } from "../reducers/cartSlice";
import { Logo, NavMenu } from "../pages/shared";

interface AlbumCardProps {
  album: Album;
  inCart: boolean;
  onAdd: (album: Album) => any;
  onRemove: (album: Album) => any;
}

const ShoppingCartWithBadge = ({ count }: { count?: number }) => {
  const navigate = useNavigate();
  const handleCart = () => {
    navigate("/cart", { replace: true });
  };

  return (
    <Box sx={{ display: "flex", flex: 1, justifyContent: "end" }}>
      <IconButton sx={{ color: "white" }} onClick={handleCart}>
        <Badge badgeContent={count} color="error">
          <ShoppingCart />
        </Badge>
      </IconButton>
    </Box>
  );
};

const AlbumCard = ({ album, inCart, onAdd, onRemove }: AlbumCardProps) => {
  return (
    <div className="card">
      <div className="card-image">
        <img src={album.cover} alt="Song cover" />
      </div>
      <div className="card-content">
        <div className="content">
          <div className="album-name">{album.album}</div>
          <div className="audio-name">{album.name}</div>
          <div className="author">{album.author}</div>
          <div className="price">$ {album.price}</div>
        </div>
        <div className="actions">
          {!inCart ? (
            <button
              className="btn-add"
              onClick={() => {
                onAdd(album);
              }}
            >
              Ajouter
            </button>
          ) : (
            <button
              className="btn-remove"
              onClick={() => {
                onRemove(album);
              }}
            >
              Supprimer
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

const SearchPage = () => {
  const navigate = useNavigate();
  useEffect(() => {
    if (!isLoggedIn()) {
      navigate("/login", { replace: true });
    }
  }, []);
  const dispatch = useAppDispatch();
  const cart = useAppSelector((state) => state.cartState.cart);
  const count = useAppSelector((state) => state.cartState.count);
  const [albums, setAlbums] = useState<Album[]>([]);
  const [searchText, setSearchText] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const handleSearchTextChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    event.preventDefault();
    setSearchText(event.target.value);
  };

  useEffect(() => {
    setIsLoading(true);
    dispatch(fetchCart());
    getAllAlbums()
      .then(({ error, value }) => {
        if (error) {
        }
        setAlbums(value);
      })
      .finally(() => setIsLoading(false));
  }, []);

  const resetSearchText = async () => {
    setIsLoading(true);
    setSearchText("");
    // Can't use searchText state variable, because for some reason it holds the old value!!!
    const { value } = await searchAlbums("");
    setAlbums(value);
    setIsLoading(false);
  };

  const search = async () => {
    setIsLoading(true);
    const { value } = await searchAlbums(searchText);
    setAlbums(value);
    setIsLoading(false);
  };

  const handleAddToCart = (album: Album) => {
    dispatch(addToCart(album));
  };

  const handleRemoveFromCart = (album: Album) => {
    dispatch(removeFromCart(album));
  };

  const isInCart = (album: Album): boolean => {
    return Boolean(cart.items?.some((item: Album) => item.id === album.id));
  };

  const handleSearchForm = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    await search();
  };

  return (
    <Box sx={{ marginTop: 10 }}>
      <AppBar position="fixed">
        <Toolbar>
          <Logo text="APPLE" />
          <Paper
            component="form"
            onSubmit={handleSearchForm}
            sx={{
              p: "2px 4px",
              display: "flex",
              alignItems: "center",
              widht: 400,
              borderRadius: "1.75rem",
              borderColor: "lightgray",
              color: "lightgray",
              backgroundColor: "white",
              flex: 1 / 2,
            }}
          >
            <InputBase
              sx={{
                ml: 1,
                flex: 1,
                color: "gray",
              }}
              placeholder="Recherche"
              inputProps={{ "aria-label": "Recherche" }}
              value={searchText}
              onChange={handleSearchTextChange}
              id="search-input"
            />
            <IconButton
              type="button"
              sx={{ p: "10px" }}
              aria-label="Rechercher"
              onClick={search}
              disabled={!searchText}
            >
              <SearchIcon />
            </IconButton>
            <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
            <IconButton
              type="button"
              sx={{ p: "10px" }}
              aria-label="Réinitialiser"
              disabled={!searchText}
              onClick={resetSearchText}
            >
              <Close />
            </IconButton>
          </Paper>
          <ShoppingCartWithBadge count={count} />
          <NavMenu />
        </Toolbar>
      </AppBar>
      <Divider sx={{ my: 1, color: "red" }} orientation="horizontal" />
      <Grid container spacing={4} sx={{ px: 2 }}>
        {isLoading && (
          <Grid
            container
            sx={{
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress color="primary" size={80} />
          </Grid>
        )}
        {albums?.length > 0
          ? albums.map((album) => (
              <Grid item sm={12} md={6} lg={4} key={album.id}>
                <AlbumCard
                  album={album}
                  inCart={isInCart(album)}
                  onAdd={handleAddToCart}
                  onRemove={handleRemoveFromCart}
                />
              </Grid>
            ))
          : !isLoading && (
              <Grid
                container
                sx={{
                  justifyContent: "center",
                  alignItems: "center",
                  marginY: "2rem",
                }}
              >
                <Typography variant="h3">Il n'y a aucun résultat</Typography>
              </Grid>
            )}
      </Grid>
    </Box>
  );
};

export default SearchPage;
