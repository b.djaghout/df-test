import env from "../environment";

export interface TokenData {
  id: string;
  userId: string;
  createdAt: Date;
  ttl: number;
  lastAccess: Date;
  firstname: string;
  lastname: string;
}

export interface SignUpData {
  firstname: string;
  lastname: string;
  phone: string;
  email: string;
  username: string;
  password: string;
}

export const login = async (
  username: any,
  password: any
): Promise<{ error?: any; value?: any }> => {
  try {
    const res = await fetch(`${env.api}/login`, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify({ username, password }),
    });
    const value = await res.json();
    return { value };
  } catch (error) {
    return { error };
  }
};

export const signUp = async (
  signUpData: SignUpData
): Promise<{ error?: any; value?: any }> => {
  try {
    const res = await fetch(`${env.api}/register`, {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(signUpData),
    });
    const value = await res.json();
    return { value };
  } catch (error) {
    return { error };
  }
};

export const getToken = (): TokenData | null => {
  const stringToken = localStorage.getItem("token");
  if (!stringToken) {
    return null;
  }
  return JSON.parse(stringToken) as TokenData;
};

const clearToken = () => {
  localStorage.removeItem("token");
};

export const logout = async () => {
  try {
    const token = getToken();
    if (token) {
      await fetch(`${env.api}/logout`, {
        method: "POST",
        headers: {
          "content-type": "application/json",
          Authorization: token.id,
        },
      });
      return clearToken();
    } else {
      return clearToken();
    }
  } catch (error) {
    return { error };
  }
};

export const saveLoginData = (tokenData: TokenData) => {
  localStorage.setItem("token", JSON.stringify(tokenData));
};

export const isLoggedIn = (): boolean => {
  const tokenString = localStorage.getItem("token");
  if (!tokenString) return false;
  const token = JSON.parse(tokenString) as TokenData;
  return !!token.id;
};
