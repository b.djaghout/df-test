import React, { ComponentType, FormEvent, useState } from "react";
import {
  AlertColor,
  Avatar,
  Box,
  Button,
  Container,
  createTheme,
  CssBaseline,
  Grid,
  SlideProps,
  Snackbar,
  TextField,
  ThemeProvider,
  Typography,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { LockOutlined } from "@mui/icons-material";
import { saveLoginData, signUp, SignUpData } from "./authService";
import MuiAlert, { AlertProps } from "@mui/material/Alert";
import Slide from "@mui/material/Slide";

type TransitionProps = Omit<SlideProps, "direction">;

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

function TransitionUp(props: TransitionProps) {
  return <Slide {...props} direction="up" />;
}

const SignUpPage = () => {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [severity, setSeverity] = useState<AlertColor | undefined>(undefined);
  const [transition, setTransition] = useState<
    ComponentType<TransitionProps> | undefined
  >(undefined);
  const theme = createTheme();
  const navigate = useNavigate();
  const handleSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    const signUpData: SignUpData = {
      firstname: String(data.get("firstname")),
      lastname: String(data.get("lastname")),
      phone: String(data.get("phone")),
      email: String(data.get("email")),
      username: String(data.get("username")),
      password: String(data.get("password")),
    };
    const { error, value } = await signUp(signUpData);
    if (error) {
      setMessage("Prolème au niveau du serveur");
      setTransition(() => TransitionUp);
      setSeverity("error");
      setOpen(true);
      return;
    }
    if (!value.userId) {
      setMessage(value.message);
      setTransition(() => TransitionUp);
      setSeverity("error");
      setOpen(true);
      return;
    }
    setMessage(value.message);
    setTransition(() => TransitionUp);
    setSeverity("success");
    setOpen(true);
    saveLoginData(value);
    navigate("/");
  };

  const handleCancel = () => {
    navigate("/login", { replace: true });
  };

  const handleClose = () => {
    setSeverity(undefined);
    setMessage("");
    setOpen(false);
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="md">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar
            sx={{ m: 1, bgcolor: "secondary.main", width: 48, height: 48 }}
          >
            <LockOutlined />
          </Avatar>
          <Typography component="h1" variant="h3">
            S'inscrire
          </Typography>
          <Box component="form" onSubmit={handleSubmit} sx={{ mt: 2 }}>
            <Grid container spacing={2} sx={{ mb: 2 }}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="fname"
                  name="firstname"
                  label="Prénom"
                  variant="outlined"
                  required
                  fullWidth
                  id="firstname"
                  autoFocus
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="lname"
                  name="lastname"
                  label="Nom"
                  variant="outlined"
                  required
                  fullWidth
                  id="lastname"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="phone"
                  name="phone"
                  label="Téléphone"
                  variant="outlined"
                  required
                  fullWidth
                  id="phone"
                  inputProps={{ pattern: "0[5-7][4-9][0-9]{7}", maxLength: 10 }}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="email"
                  name="email"
                  label="Email"
                  variant="outlined"
                  required
                  fullWidth
                  type="email"
                  id="email"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="username"
                  name="username"
                  label="Nom d'utilisateur"
                  variant="outlined"
                  required
                  fullWidth
                  id="username"
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="password"
                  name="password"
                  label="Mot de passe"
                  variant="outlined"
                  required
                  fullWidth
                  type="password"
                  id="password"
                  inputProps={{minLength: 6}}
                />
              </Grid>
            </Grid>
            <Grid container justifyContent="space-around" spacing={{ xs: 2 }}>
              <Grid item xs={12} sm={5}>
                <Button
                  variant="outlined"
                  fullWidth
                  color="error"
                  onClick={handleCancel}
                >
                  Annuler
                </Button>
              </Grid>
              <Grid item xs={12} sm={5}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                >
                  S'inscrire
                </Button>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Snackbar
          open={open}
          onClose={handleClose}
          TransitionComponent={transition}
          autoHideDuration={2500}
        >
          <Alert severity={severity}>{message}</Alert>
        </Snackbar>
      </Container>
    </ThemeProvider>
  );
};

export default SignUpPage;
