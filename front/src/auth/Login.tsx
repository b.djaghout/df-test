import React, { useEffect, useState } from "react";
import {
  Alert,
  Avatar,
  Box,
  Button,
  Container,
  CssBaseline,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import ThemeProvider from "@mui/material/styles/ThemeProvider";
import createTheme from "@mui/material/styles/createTheme";
import { LockOutlined } from "@mui/icons-material";
import { isLoggedIn, login, saveLoginData } from "./authService";

const LoginPage = () => {
  const navigate = useNavigate();
  const [message, setMessage] = useState("");

  useEffect(() => {
    if (isLoggedIn()) {
      navigate("/", { replace: true });
    }
  }, []);

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    const { error, value } = await login(
      data.get("email"),
      data.get("password")
    );
    if (error) {
      setMessage("Prolème au niveau du serveur");
      return;
    }
    if (!value.userId) {
      setMessage(value.message);
      return;
    }
    saveLoginData(value);
    navigate("/");
  };

  const theme = createTheme();

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar
            sx={{ m: 1, bgcolor: "secondary.main" }}
            src={"../assets/images/apple.png"}
          >
            <LockOutlined />
          </Avatar>
          <Typography component="h1" variant="h5">
            Se connecter
          </Typography>
          <Box component="form" onSubmit={handleSubmit} sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              value="admin"
              required
              fullWidth
              id="email"
              label="Email / Username"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              margin="normal"
              value="123456"
              required
              fullWidth
              name="password"
              label="Mot de passe"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Se connecter
            </Button>
            <Grid container>
              <Grid item>
                <Link
                  to="/sign-up"
                  style={{ textDecoration: "none", color: "dodgerblue" }}
                >
                  Vous n'avez pas de compte ? S'inscrire!
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
        {message && <Alert severity="error">{message}</Alert>}
      </Container>
    </ThemeProvider>
  );
};

export default LoginPage;
