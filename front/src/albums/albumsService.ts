import env from "../environment";
import { getToken } from "../auth/authService";
export const getAllAlbums = async (searchText: string = ''): Promise<{ error?: any; value?: any }> => {
  try {
    const accessToken = getToken();
    const res = await fetch(`${env.api}/audios?search=${searchText}`, {
      method: "GET",
      headers: {
        "content-type": "application/json",
        Authorization: `Bearer ${accessToken?.id}`,
      },
    });
    const value = await res.json();
    return { value };
  } catch (error) {
    return { error };
  }
};

export const searchAlbums = getAllAlbums;
