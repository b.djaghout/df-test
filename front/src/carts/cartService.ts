import env from "../environment";
import { getToken } from "../auth/authService";
import { Album } from "../models/Album";

export const getMyCart = async (): Promise<{ error?: any; value?: any }> => {
  try {
    const headers = {
      "content-type": "application/json",
      authorization: `bearer ${getToken()?.id}`,
    };
    const res = await fetch(`${env.api}/carts/my-cart`, {
      method: "GET",
      headers,
    });
    const value = await res.json();
    return { value };
  } catch (error) {
    return { error };
  }
};

export const createCart = async (album: Album) => {
  try {
    const headers = {
      "content-type": "application/json",
      authorization: `bearer ${getToken()?.id}`,
    };
    const cartData = {
      userId: getToken()?.userId,
      items: [album.id],
    };
    const res = await fetch(`${env.api}/carts`, {
      method: "POST",
      body: JSON.stringify(cartData),
      headers,
    });
    const value = await res.json();
    return { value };
  } catch (error) {
    return { error };
  }
};

export const addItemToCart = async (cartId: string, audioId: string) => {
  try {
    const headers = {
      "content-type": "application/json",
      authorization: `bearer ${getToken()?.id}`,
    };
    const res = await fetch(`${env.api}/carts/${cartId}/items/${audioId}`, {
      method: "POST",
      headers,
    });
    const value = await res.json();
    return { value };
  } catch (error) {
    return { error };
  }
};

export const removeItemFromCart = async (cartId: string, audioId: string) => {
  try {
    const headers = {
      "content-type": "application/json",
      authorization: `bearer ${getToken()?.id}`,
    };
    const res = await fetch(`${env.api}/carts/${cartId}/items/${audioId}`, {
      method: "DELETE",
      headers,
    });
    const value = await res.json();
    return { value };
  } catch (error) {
    return { error };
  }
};

export const checkoutCart = async (cartId: string, address: string) => {
  try {
    const headers = {
      "content-type": "application/json",
      authorization: `bearer ${getToken()?.id}`,
    };
    const res = await fetch(
      `${env.api}/carts/${cartId}/checkout?address=${address}`,
      {
        method: "POST",
        headers,
      }
    );
    const value = await res.json();
    return { value };
  } catch (error) {
    return { error };
  }
};

export const getMyCheckout = async () => {
  try {
    const headers = {
      "content-type": "application/json",
      authorization: `bearer ${getToken()?.id}`,
    };
    const res = await fetch(`${env.api}/carts/checkout`, {
      method: "GET",
      headers,
    });
    const value = await res.json();
    return { value };
  } catch (error) {
    return { error };
  }
};
