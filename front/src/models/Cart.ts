import {Album} from './Album';

export interface Cart {
  id: string;
  items?: Array<Album>,
  totalPrice: number;
  status: "PENDING" | "CANCELED" | "PURCHASED";
  owner: string;
  checkoutAddress?: string;
  checkoutAt?: string;
  createdAt?: string;
}
