export interface Album {
  id: string;
  name: string;
  album: string;
  author: string;
  price: string;
  cover: string;
}
