import React from "react";
import "./App.css";
import SearchPage from "./search-page/SearchPage";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./auth/Login";
import NotFound from "./pages/NotFound";
import SignUpPage from "./auth/SignUp";
import CartPage from "./pages/CartPage";
import HistoryPage from "./pages/HistoryPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/sign-up" element={<SignUpPage />} />
        <Route path="/" element={<SearchPage />} />
        <Route path="/cart" element={<CartPage />} />
        <Route path="/history" element={<HistoryPage />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
