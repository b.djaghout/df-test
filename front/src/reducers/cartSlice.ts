import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { Album } from "../models/Album";
import { PayloadAction } from "@reduxjs/toolkit/dist/createAction";
import { Cart } from "../models/Cart";
import {
  addItemToCart,
  createCart,
  getMyCart,
  removeItemFromCart,
  checkoutCart,
} from "../carts/cartService";

interface CartState {
  cart: Cart;
  count: number;
  loading: boolean;
}

const initialState: CartState = {
  cart: { id: "", owner: "", items: [], status: "PENDING", totalPrice: 0, createdAt: "" },
  count: 0,
  loading: false,
};

export const createCartThunk = createAsyncThunk<Cart, Album>(
  "cartState/createCart",
  async (album: Album) => {
    const { value } = await createCart(album);
    return value as Cart;
  }
);

export const fetchCart = createAsyncThunk<Cart>(
  "cartState/fetchCart",
  async () => {
    const { value } = await getMyCart();
    return value as Cart;
  }
);

export const addToCart = createAsyncThunk(
  "cartState/addToCart",
  async (album: Album, { getState }) => {
    const state: any = getState();
    const cartId = state.cartState.cart.id;
    if (!cartId) {
      const { value } = await createCart(album);
      return value;
    }
    const { value } = await addItemToCart(cartId, album.id);
    return value;
  }
);

export const removeFromCart = createAsyncThunk(
  "cartState/removeFromCart",
  async (album: Album, { getState }) => {
    const state: any = getState();
    const cartId = state.cartState.cart.id;
    const { value } = await removeItemFromCart(cartId, album.id);
    return value;
  }
);

export const cartCheckout = createAsyncThunk(
  "cartState/cartCheckout",
  async ({ cartId, address }: { cartId: string; address: string }) => {
    console.log({ cartId });
    const { value } = await checkoutCart(cartId, address);
    return value;
  }
);

const setCartReducer = (state: CartState, action: PayloadAction<Cart>) => {
  state.cart = action.payload;
  state.count = action.payload.items?.length || 0;
};

const removeAllFromCartReducer = (state: CartState) => {
  state.cart = initialState.cart;
  state.count = 0;
};

export const cartSlice = createSlice({
  name: "cartState",
  initialState,
  reducers: {
    setCart: setCartReducer,
    removeAllFromCart: removeAllFromCartReducer,
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCart.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(fetchCart.fulfilled, (state, { payload }) => {
      state.cart = payload;
      state.count = payload.items?.length || 0;
      state.loading = false;
    });
    builder.addCase(createCartThunk.pending, (state: CartState) => {
      state.loading = true;
    });
    builder.addCase(createCartThunk.fulfilled, (state, { payload }) => {
      state.cart = payload;
      state.count = payload.items?.length || 0;
      state.loading = false;
    });
    builder.addCase(addToCart.pending, (state: CartState) => {
      state.loading = true;
    });
    builder.addCase(addToCart.fulfilled, (state: CartState, { payload }) => {
      state.cart = payload;
      state.count = payload.items.length;
      state.loading = false;
    });
    builder.addCase(removeFromCart.pending, (state: CartState) => {
      state.loading = true;
    });
    builder.addCase(
      removeFromCart.fulfilled,
      (state: CartState, { payload }) => {
        state.cart = payload;
        state.count = payload.items.length;
        state.loading = false;
      }
    );
    builder.addCase(cartCheckout.pending, (state: CartState) => {
      state.loading = true;
    });
    builder.addCase(cartCheckout.fulfilled, (state: CartState) => {
      state.cart = initialState.cart;
      state.count = initialState.count;
      state.loading = false;
    });
  },
});

export const { removeAllFromCart, setCart } = cartSlice.actions;

export default cartSlice.reducer;
